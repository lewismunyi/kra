#include "classheader.h"
#include<iostream>
#include<string.h>
#include<stdlib.h>

using namespace std;

vehicle::vehicle(int V=3330,int Vp=0030,int engine=1500,int Yr=2014,int val=1300000,int PIN=50005, char* man="TOYOTA",char* body="SEDAN")
{
    vehicle::ViD=V;
    vehicle::Vplate=Vp;
    vehicle::engineCapacity=engine;
    vehicle::YrOfMan=Yr;
    vehicle::value=val;
    vehicle::OwnerPIN=PIN;
    vehicle::manufacturer=man;
    vehicle::bodytype=*body;
    vehicle::KRARevenue=0;
}

/*ordinary::ordinary(int ViD,int Vplate,int engineCapacity,int YrOfMan,int value,int OwnerPIN, char* manufacturer, char* bodytype, int vtype, double trate):vehicle(ViD,Vplate,engineCapacity,YrOfMan,value,OwnerPIN,manufacturer,bodytype),taxrate(trate),vehicleType(vtype)
{

vtype=1;
trate=0.01;
}*/




void vehicle::display()
{
     cout<<"======================================================"<<endl;
    cout<<"=============       VEHICLE DETAILS       ==========="<<endl;
    cout<<"======================================================"<<endl;
     cout<<"Vehicle ID:\t";
        cout<<ViD<<endl;
         cout<<" Owned By Holder of PIN \t";
        cout<<OwnerPIN<<endl;
        cout<<"Vehicle Number plate \t";
        cout<<Vplate<<endl;
        cout<<"Engine capacity \t";
        cout<<engineCapacity<<endl;
        cout<<"Manufactured by \t";
        cout<<manufacturer<<endl;
        cout<<" In the year\t";
        cout<<YrOfMan<<endl;
        cout<<" Priced at\t";
        cout<<"Ksh "<<value;

}


void vehicle::computeKRARevenue()
{
    int delta;
    if((2017-YrOfMan)<4)
    {
        delta=0.0;
     if(engineCapacity<=1000)
        {
            vehicle::KRARevenue=vehicle::value*(delta+0.1);
        }
        else if(vehicle::engineCapacity>1000 && vehicle::engineCapacity<=2000)
        {
            vehicle::KRARevenue=vehicle::value*(delta+0.15);
        }
        else if ( vehicle::engineCapacity>2000 && vehicle::engineCapacity<=3000)
        {
            vehicle::KRARevenue=vehicle::value*(delta+0.18);

        }
        else if (vehicle::engineCapacity>3000 && vehicle::engineCapacity<=4000)
        {
            vehicle::KRARevenue=vehicle::value*(delta+0.2);
        }
        else if (vehicle::engineCapacity>4000)
        {
            vehicle::KRARevenue=vehicle::value*(delta+0.22);
        }

    }

    else

    {
        delta=0.09;

        if(vehicle::engineCapacity<=1000)
        {
            vehicle::KRARevenue=vehicle::value*(delta+0.1);
        }
        else if(vehicle::engineCapacity>1000 && vehicle::engineCapacity<=2000)
        {
            vehicle::KRARevenue=vehicle::value*(delta+0.15);
        }
        else if ( vehicle::engineCapacity>2000 && vehicle::engineCapacity<=3000)
        {
            vehicle::KRARevenue=vehicle::value*(delta+0.18);

        }
        else if (vehicle::engineCapacity>3000 && vehicle::engineCapacity<=4000)
        {
            vehicle::KRARevenue=vehicle::value*(delta+0.2);
        }
        else if (vehicle::engineCapacity>4000)
        {
            vehicle::KRARevenue=vehicle::value*(delta+0.22);
        }

    }
}

void vehicle::computeKRARevenue(double importduty)
{
    int delta;
    if((2017-YrOfMan)<4)
    {
        delta=0.0 + importduty;
     if(engineCapacity<=1000)
        {
            vehicle::KRARevenue=vehicle::value*(delta+0.1);
        }
        else if(vehicle::engineCapacity>1000 && vehicle::engineCapacity<=2000)
        {
            vehicle::KRARevenue=vehicle::value*(delta+0.15);
        }
        else if ( vehicle::engineCapacity>2000 && vehicle::engineCapacity<=3000)
        {
            vehicle::KRARevenue=vehicle::value*(delta+0.18);

        }
        else if (vehicle::engineCapacity>3000 && vehicle::engineCapacity<=4000)
        {
            vehicle::KRARevenue=vehicle::value*(delta+0.2);
        }
        else if (vehicle::engineCapacity>4000)
        {
            vehicle::KRARevenue=vehicle::value*(delta+0.22);
        }

    }

    else

    {
        delta=0.09 +importduty;

        if(vehicle::engineCapacity<=1000)
        {
            vehicle::KRARevenue=vehicle::value*(delta+0.1);
        }
        else if(vehicle::engineCapacity>1000 && vehicle::engineCapacity<=2000)
        {
            vehicle::KRARevenue=vehicle::value*(delta+0.15);
        }
        else if ( vehicle::engineCapacity>2000 && vehicle::engineCapacity<=3000)
        {
            vehicle::KRARevenue=vehicle::value*(delta+0.18);

        }
        else if (vehicle::engineCapacity>3000 && vehicle::engineCapacity<=4000)
        {
            vehicle::KRARevenue=vehicle::value*(delta+0.2);
        }
        else if (vehicle::engineCapacity>4000)
        {
            vehicle::KRARevenue=vehicle::value*(delta+0.22);
        }

    }
}


void vehicle::generateInvoice()
{
     cout<<"======================================================"<<endl;
    cout<<"=============       VEHICLE INVOICE       ==========="<<endl;
    cout<<"======================================================"<<endl;
    cout<<"\t You are required to pay the below amount to KRA"<<endl;
     cout<<"Vehicle ID:\t";
        cout<<ViD<<endl;
         cout<<" Owned By Holder of PIN \t";
        cout<<OwnerPIN<<endl;
        cout<<"Vehicle Number plate \t";
        cout<<Vplate<<endl;
         cout<<" Priced at\t";
        cout<<"Ksh***************** "<<KRARevenue;
         cout<<"======================================================"<<endl;
    cout<<"=============       END OF INVOICE       ==========="<<endl;
    cout<<"======================================================"<<endl;


}


void ordinary::display()
{
     cout<<"======================================================"<<endl;
    cout<<"=============       NON TRUCK VEHICLES      ==========="<<endl;
    cout<<"======================================================"<<endl;
    cout<<"=====================ORDINARY CATEGORY================="<<endl;
    cout<<"======================================================"<<endl;
    cout<<"==============VEHICLE TYPE=============="<<endl;
    cout<<"====================="<<ordinary::vehicleType;
    cout<<"==============="<<endl;

     cout<<"Vehicle ID:\t";
        cout<<ViD<<endl;
         cout<<" Owned By Holder of PIN \t";
        cout<<OwnerPIN<<endl;
        cout<<"Vehicle Number plate \t";
        cout<<Vplate<<endl;
        cout<<"Engine capacity \t";
        cout<<engineCapacity<<endl;
        cout<<"Manufactured by \t";
        cout<<manufacturer<<endl;
        cout<<" In the year\t";
        cout<<YrOfMan<<endl;
        cout<<" Priced at\t";
        cout<<"Ksh "<<value;


}


void motorbike::display()
{
     cout<<"======================================================"<<endl;
    cout<<"=============       VEHICLE DETAILS       ==========="<<endl;
    cout<<"======================================================"<<endl;
    cout<<"=====================MOTOR CYCLE CATEGORY================="<<endl;
    cout<<"====================BODY TYPE: 2 WHEELER============="<<endl;
         cout<<"MotorCycle ID:\t";
        cout<<ViD<<endl;
         cout<<" Owned By Holder of PIN \t";
        cout<<OwnerPIN<<endl;
        cout<<"MotorCycle Number plate \t KMTC ";
        cout<<Vplate<<endl;
        cout<<"MotorCycle capacity \t";
        cout<<engineCapacity<<endl;
        cout<<"Manufactured by \t";
        cout<<manufacturer<<endl;
        cout<<" In the year\t";
        cout<<YrOfMan<<endl;
        cout<<" Priced at\t";
        cout<<"Ksh "<<value;


}
void truck::display()
{

  cout<<"======================================================"<<endl;
    cout<<"=============       VEHICLE DETAILS       ==========="<<endl;
    cout<<"======================================================"<<endl;
    cout<<"=====================TRUCK CATEGORY================="<<endl;
    cout<<"====================BODY TYPE: 6 AXLES============="<<endl;
         cout<<"TRUCK ID:\t";
        cout<<ViD<<endl;
         cout<<" Owned By Holder of PIN \t";
        cout<<OwnerPIN<<endl;
        cout<<"TRUCK Number plate \t T ";
        cout<<Vplate<<endl;
        cout<<"TRAILER Number plate \t ZX ";
        cout<<TrailerPlate<<endl;
        cout<<"Truck Engine capacity \t";
        cout<<engineCapacity<<endl;
        cout<<"Manufactured by \t";
        cout<<manufacturer<<endl;
        cout<<" In the year\t";
        cout<<YrOfMan<<endl;
        cout<<" Priced at\t";
        cout<<"Ksh "<<value;


}

void motorbike::computeKRARevenue()
{
    taxrate=0.18;
    KRARevenue=taxrate*value;
}

void motorbike::computeKRARevenue(double importduty)
{
    taxrate=0.18+importduty;
    KRARevenue=taxrate*value;
}

