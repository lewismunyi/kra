#ifndef CLASSHEADER_H_INCLUDED
#define CLASSHEADER_H_INCLUDED
#include<iostream>
#include<string.h>
#include<stdlib.h>

using namespace std;
class vehicle
{
protected:
    int ViD,Vplate,engineCapacity,YrOfMan,value,OwnerPIN,KRARevenue;
    char * manufacturer,bodytype;

public:
    vehicle(int ViD,int Vplate,int engineCapacity,int YrOfMan,int value,int OwnerPIN, char* manufacturer, char* bodytype);
    void registerVehicle();
    bool vehicleSearch(int ViD);
    void computeKRARevenue();
    void computeKRARevenue(double importduty); //overloading
    void generateInvoice(); //calculate and display
    void display();

};

//derived class from vehicle
class ordinary: public vehicle
{
private:
    int vehicleType;
    double taxrate;

public:
    ordinary(int ViD,int Vplate,int engineCapacity,int YrOfMan,int value,int OwnerPIN, char* manufacturer, char* bodytype, int vtype, double trate):vehicle(ViD,Vplate,engineCapacity,YrOfMan,value,OwnerPIN,manufacturer,bodytype),taxrate(trate),vehicleType(vtype){};
    void display(); //polymorphism of display will be showing logbook
};


//derived class from vehicle
class motorbike: public vehicle
{
private:
    double taxrate;

public:
   motorbike(int ViD,int Vplate,int engineCapacity,int YrOfMan,int value,int OwnerPIN, char* manufacturer, char* bodytype,double trate):vehicle(ViD,Vplate,engineCapacity,YrOfMan,value,OwnerPIN,manufacturer,bodytype),taxrate(trate){};
   void computeKRARevenue();//override default function by using flat rate calculation
    void computeKRARevenue(double importduty); //overload
    void display();  //polymorphism of display will be showing logbook
};


//derived class from vehicle
class truck: public vehicle
 {
private:
    int axleNum,TrailerPlate;
    double taxrate;

public:
     truck(int ViD,int Vplate,int engineCapacity,int YrOfMan,int value,int OwnerPIN, char* manufacturer, char* bodytype, int axleNum,int TrailerPlate, double trate):vehicle(ViD,Vplate,engineCapacity,YrOfMan,value,OwnerPIN,manufacturer,bodytype),axleNum(axleNum),TrailerPlate(TrailerPlate),taxrate(trate){};
    void display(); //polymorphism of display will be showing logbook
};


#endif // CLASSHEADER_H_INCLUDED

